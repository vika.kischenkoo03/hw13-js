// 1. Функція setTimeout() виконується один раз, через зазначений проміжок часу.
//    функція setInterval() виконується періодично через зазначений інтервал часу.

// 2.Вона не буде працювати миттєво. При нульовій затримці виклик функції
// буде заплановано одразу після виконання поточного коду.

// 3.Тому що зовнішні змінні існують в пам'яті до того часу, поки існує функція,
// вони можуть займати більше пам'ті, ніж функція.

const btnStop = document.querySelector(".btn_stop");
const btnStart = document.querySelector(".btn_start");
let images = document.querySelectorAll(".image-to-show");

let slideIndex = 0;
let maxIndex = images.length - 1;

btnStart.disabled = true;

let timerId = null;
const delay = 3000;

let showSlides = (delay) => {
  timerId = setTimeout(() => {
    images[slideIndex].classList.toggle("hidden");
    slideIndex = slideIndex >= maxIndex ? 0 : ++slideIndex;

    images[slideIndex].classList.toggle("hidden");

    showSlides(delay);
  }, delay);
};

showSlides(delay);

btnStop.addEventListener("click", () => {
  clearTimeout(timerId);
  btnStart.removeAttribute("disabled");
  btnStop.setAttribute("disabled", true);
});

btnStart.addEventListener("click", () => {
  showSlides(delay);
  btnStart.setAttribute("disabled", true);
  btnStop.removeAttribute("disabled");
});
